﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    BoxCollider swordBox;
    public float spacing;
    public int castAmount = 4;

    public LayerMask collisionMask;

    public float rayLength;
    public Vector3 colliderCenter;
    public float offset;


    void Awake() {
        swordBox = GetComponent<BoxCollider>();
    }

    void Start() {
        CalculateSpacing();

    }

    void FixedUpdate() {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;


        for (int i = 0; i < castAmount; i++) {

            Vector3 rayOrigin = transform.position + (-transform.up* offset);

            rayOrigin += transform.up * (spacing * i);

            RaycastHit hit;
            if (Physics.Raycast(rayOrigin, transform.forward, out hit, rayLength, collisionMask))
            {

                Debug.DrawRay(rayOrigin, transform.forward * rayLength, Color.red);
                Debug.Log("hit!!!!!!");
            }
            else {
                Debug.DrawRay(rayOrigin, transform.forward * rayLength, Color.blue);
                Debug.Log("not hit!!!");

            }
        } 
    }

    void CalculateSpacing() {
        Bounds swordBounds = swordBox.bounds;
        spacing = swordBounds.size.y / (castAmount - 1);

        colliderCenter = new Vector3(swordBounds.center.x, swordBounds.center.y, swordBounds.center.z);
        offset = swordBounds.center.y - swordBounds.min.y;
    }

}
